use super::literal::*;
use super::model::*;

///PropositionTree describes any propositional logic in any of its form
///A proposition can be either a literal, the negation of a proposition,
///a binary operation on two propositions.
#[derive(Clone, Debug)]
pub enum BinOp {
    And,
    Or,
    Xor,
    Implication,
    Equivalence,
}

///PropositionTree represents a general logical proposition.
#[derive(Clone, Debug)]
pub enum PropositionTree {
    Literal(Literal),
    Not(Box<PropositionTree>),
    BinOp(BinOp, Box<PropositionTree>, Box<PropositionTree>),
}

///Exhaustively tests the equivalence of two propositions
pub fn equivalent<P: Proposition, Q: Proposition>(p1: P, p2: Q) -> bool {
    let size = p1.size();
    if size != p2.size() {
        false
    } else {
        let models = Model::enumerate_with_size(size);
        models.iter().all(|i| !(p1.eval(i) ^ p2.eval(i)))
    }
}

impl PropositionTree {
    /************* Constructors ************/
    pub fn new_true() -> PropositionTree {
        PropositionTree::Literal(Literal::new_true())
    }
    pub fn new_false() -> PropositionTree {
        PropositionTree::Literal(Literal::new_false())
    }
    pub fn literal(lit: usize) -> PropositionTree {
        PropositionTree::Literal(Literal::Variable(lit as isize))
    }
    pub fn not(prop: PropositionTree) -> PropositionTree {
        PropositionTree::Not(Box::new(prop))
    }
    pub fn and(prop1: PropositionTree, prop2: PropositionTree) -> PropositionTree {
        PropositionTree::BinOp(BinOp::And, Box::new(prop1), Box::new(prop2))
    }
    pub fn or(prop1: PropositionTree, prop2: PropositionTree) -> PropositionTree {
        PropositionTree::BinOp(BinOp::Or, Box::new(prop1), Box::new(prop2))
    }
    pub fn xor(prop1: PropositionTree, prop2: PropositionTree) -> PropositionTree {
        PropositionTree::BinOp(BinOp::Xor, Box::new(prop1), Box::new(prop2))
    }
    pub fn implication(prop1: PropositionTree, prop2: PropositionTree) -> PropositionTree {
        PropositionTree::BinOp(BinOp::Implication, Box::new(prop1), Box::new(prop2))
    }
    pub fn equivalence(prop1: PropositionTree, prop2: PropositionTree) -> PropositionTree {
        PropositionTree::BinOp(BinOp::Equivalence, Box::new(prop1), Box::new(prop2))
    }

    ///Returns the size of the proposition tree, including nodes and leaves
    pub fn len(&self) -> usize {
        match self {
            PropositionTree::Literal(_) => 1,
            PropositionTree::Not(p) => 1 + p.size(),
            PropositionTree::BinOp(_, p1, p2) => 1 + p1.size() + p2.size(),
        }
    }

    ///Returns the maximal literal id found in the proposition
    pub fn max_literal(&self) -> usize {
        match self {
            PropositionTree::Literal(lit) => lit.size(),
            PropositionTree::Not(p) => p.max_literal(),
            PropositionTree::BinOp(_, p1, p2) => std::cmp::max(p1.max_literal(), p2.max_literal()),
        }
    }
}

use super::Proposition;
impl Proposition for PropositionTree {
    ///Evaluates a proposition regarding to a given interpretation.
    fn eval(&self, m: &Model) -> bool {
        match self {
            PropositionTree::Literal(lit) => lit.eval(m),
            PropositionTree::Not(p) => !p.eval(m),
            PropositionTree::BinOp(op, p1, p2) => {
                let (e1, e2) = (p1.eval(m), p2.eval(m));
                match op {
                    BinOp::And => e1 && e2,
                    BinOp::Or => e1 || e2,
                    BinOp::Xor => e1 ^ e2,
                    BinOp::Implication => !e1 || e2,
                    BinOp::Equivalence => !(e1 ^ e2),
                }
            }
        }
    }

    ///Evaluates a proposition regarding to a given interpretation.
    fn partial_eval(&self, m: &PartialModel) -> Option<bool> {
        match self {
            PropositionTree::Literal(lit) => lit.partial_eval(m),
            PropositionTree::Not(p) => Some(!p.partial_eval(m)?),
            PropositionTree::BinOp(op, p1, p2) => {
                let (e1, e2) = (p1.partial_eval(m)?, p2.partial_eval(m)?);
                Some(match op {
                    BinOp::And => e1 && e2,
                    BinOp::Or => e1 || e2,
                    BinOp::Xor => e1 ^ e2,
                    BinOp::Implication => !e1 || e2,
                    BinOp::Equivalence => !(e1 ^ e2),
                })
            }
        }
    }

    ///Returns the number of literals used in the proposition
    fn size(&self) -> usize {
        match self {
            PropositionTree::Literal(lit) => lit.size(),
            PropositionTree::Not(p) => p.size(),
            PropositionTree::BinOp(_, p1, p2) => std::cmp::max(p1.size(), p2.size()),
        }
    }
}

/************** Unit tests ****************/
#[cfg(test)]
mod tests {
    use super::*;
    /**************** Evaluate a proposition ****************/
    #[test]
    fn evaluate_simple_propositions() {
        let prop = PropositionTree::and(PropositionTree::literal(1), PropositionTree::literal(2));
        assert!(prop.eval(&Model::from(&[true, true])));
        assert!(!prop.eval(&Model::from(&[true, false])));
        assert!(!prop.eval(&Model::from(&[false, true])));
        assert!(!prop.eval(&Model::from(&[false, false])));

        let prop = PropositionTree::or(PropositionTree::literal(1), PropositionTree::literal(2));
        assert!(prop.eval(&Model::from(&[true, true])));
        assert!(prop.eval(&Model::from(&[true, false])));
        assert!(prop.eval(&Model::from(&[false, true])));
        assert!(!prop.eval(&Model::from(&[false, false])));

        let prop = PropositionTree::xor(PropositionTree::literal(1), PropositionTree::literal(2));
        assert!(!prop.eval(&Model::from(&[true, true])));
        assert!(prop.eval(&Model::from(&[true, false])));
        assert!(prop.eval(&Model::from(&[false, true])));
        assert!(!prop.eval(&Model::from(&[false, false])));

        let prop =
            PropositionTree::implication(PropositionTree::literal(1), PropositionTree::literal(2));
        assert!(prop.eval(&Model::from(&[true, true])));
        assert!(!prop.eval(&Model::from(&[true, false])));
        assert!(prop.eval(&Model::from(&[false, true])));
        assert!(prop.eval(&Model::from(&[false, false])));

        let prop =
            PropositionTree::equivalence(PropositionTree::literal(1), PropositionTree::literal(2));
        assert!(prop.eval(&Model::from(&[true, true])));
        assert!(!prop.eval(&Model::from(&[true, false])));
        assert!(!prop.eval(&Model::from(&[false, true])));
        assert!(prop.eval(&Model::from(&[false, false])));
    }

    #[test]
    fn evaluate_complex_proposition() {
        let prop = PropositionTree::implication(
            PropositionTree::or(
                PropositionTree::literal(1),
                PropositionTree::not(PropositionTree::and(
                    PropositionTree::literal(2),
                    PropositionTree::literal(3),
                )),
            ),
            PropositionTree::equivalence(PropositionTree::literal(1), PropositionTree::literal(2)),
        );
        assert!(prop.eval(&Model::from(&[false, false, false])));
        assert!(prop.eval(&Model::from(&[false, false, true])));
        assert!(!prop.eval(&Model::from(&[false, true, false])));
        assert!(prop.eval(&Model::from(&[false, true, true])));
        assert!(!prop.eval(&Model::from(&[true, false, false])));
        assert!(!prop.eval(&Model::from(&[true, false, true])));
        assert!(prop.eval(&Model::from(&[true, true, false])));
        assert!(prop.eval(&Model::from(&[true, true, true])));
    }

    /************** equality trait **************/
    #[test]
    fn compare_two_equals_propositions() {
        let p1 = PropositionTree::not(PropositionTree::and(
            PropositionTree::literal(1),
            PropositionTree::literal(2),
        ));
        let p2 = PropositionTree::or(
            PropositionTree::not(PropositionTree::literal(1)),
            PropositionTree::not(PropositionTree::literal(2)),
        );
        assert!(equivalent(p1, p2));
    }

    #[test]
    fn compare_two_different_propositions() {
        let p1 = PropositionTree::not(PropositionTree::and(
            PropositionTree::literal(1),
            PropositionTree::literal(2),
        ));
        let p2 = PropositionTree::and(
            PropositionTree::not(PropositionTree::literal(1)),
            PropositionTree::not(PropositionTree::literal(2)),
        );
        assert!(!equivalent(p1, p2));
    }
}
