#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct Model {
    model: Vec<bool>,
}

impl Model {
    ///Returns a new model given a slice of booleans
    pub fn from(m: &[bool]) -> Self {
        Self { model: m.to_vec() }
    }

    ///Returns a new model initialized to false for n variables
    pub fn with_size(n: usize) -> Self {
        Self {
            model: vec![false; n],
        }
    }

    ///Returns a random model for n variables. Probability for true is 0.5
    pub fn random_with_size(n: usize) -> Self {
        Self {
            model: (0..n).map(|_| rand::random::<bool>()).collect(),
        }
    }

    ///Returns every possible model for n variables.
    ///This function return vector is length 2^n. It may panic when capacity needed exceeds isize::MAX.
    ///This function should be called only for small models.
    pub fn enumerate_with_size(n: usize) -> Vec<Self> {
        (0..(1 << n))
            .map(|k| Self {
                model: (0..n).map(|bit| k & (1 << bit) != 0).collect(),
            })
            .collect()
    }

    ///Returns the model as slice of vector of booleans
    pub fn as_slice(&self) -> &[bool] {
        self.model.as_slice()
    }

    pub fn flip(&mut self, var: usize) {
        assert_ne!(var, 0, "Trying to flip a 0 literal");
        self.model[var - 1] = !self.model[var - 1];
    }

    ///Returns the evaluation of a variable literal as a signed integer
    pub fn eval(&self, lit: isize) -> bool {
        assert_ne!(lit, 0, "Trying to evaluate a 0 literal");
        if lit < 0 {
            !self.model[-lit as usize - 1]
        } else {
            self.model[lit as usize - 1]
        }
    }

    ///Returns the number of variables evaluated
    pub fn size(&self) -> usize {
        self.model.len()
    }
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct PartialModel {
    model: Vec<Option<bool>>,
}

impl PartialModel {
    ///Returns a partial model with given number of variables
    pub fn with_size(n: usize) -> Self {
        Self {
            model: vec![None; n],
        }
    }

    ///Converts a model to a partial model fully decided
    pub fn from_model(m: Model) -> Self {
        Self {
            model: m.as_slice().iter().map(|val| Some(*val)).collect(),
        }
    }

    pub fn set(&mut self, var: isize) {
        self.model[var.abs() as usize - 1] = Some(var > 0);
    }

    ///Converts the partial model to a complete model, assuming every non evaluated variable to be
    ///false
    pub fn to_model(self) -> Model {
        Model::from(
            self.model
                .iter()
                .map(|val| val.unwrap_or(false))
                .collect::<Vec<bool>>()
                .as_slice(),
        )
    }

    pub fn flip(&mut self, var: usize) {
        assert_ne!(var, 0, "Trying to flip a 0 literal");
        if let Some(val) = self.model[var - 1] {
            self.model[var - 1] = Some(!val);
        }
    }

    ///Chooses the next non evaluated variable to be evaluated with given value
    ///Returns the chosen variable if one could be chosen, None otherwise.
    pub fn choose_next(&mut self, val: bool) -> Option<usize> {
        let chosen_var =
            self.model
                .iter()
                .enumerate()
                .find_map(|(var, val)| if val.is_none() { Some(var) } else { None });
        if let Some(chosen_var) = chosen_var {
            self.model[chosen_var] = Some(val);
            Some(chosen_var + 1)
        } else {
            None
        }
    }

    ///Returns true if the literal could be evaluated as true, false otherwise
    pub fn eval(&self, lit: isize) -> bool {
        assert_ne!(lit, 0, "Trying to evaluate a 0 literal");
        if let Some(val) = self.model[lit.abs() as usize - 1] {
            (lit > 0 && val) || (lit < 0 && !val)
        } else {
            false
        }
    }

    ///Returns a partial evaluation of the literal
    pub fn partial_eval(&self, lit: isize) -> Option<bool> {
        assert_ne!(lit, 0, "Trying to evaluate a 0 literal");
        if let Some(val) = self.model[lit.abs() as usize - 1] {
            Some((lit > 0 && val) || (lit < 0 && !val))
        } else {
            None
        }
    }

    ///Returns the number of variables partially evaluated
    pub fn size(&self) -> usize {
        self.model.len()
    }
}

use std::ops::Index;
impl Index<usize> for Model {
    type Output = bool;

    fn index(&self, idx: usize) -> &Self::Output {
        assert_ne!(idx, 0, "Trying to evaluate a 0 literal");
        &self.model[idx - 1]
    }
}

impl Index<usize> for PartialModel {
    type Output = Option<bool>;

    fn index(&self, idx: usize) -> &Self::Output {
        assert_ne!(idx, 0, "Trying to evaluate a 0 literal");
        &self.model[idx - 1]
    }
}

/********************* Unit tests *********************/
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn generate_al_models_with_size() {
        //Try to generate all models with 4 variables.
        let n = 4;
        let mut models = Model::enumerate_with_size(n);

        models.sort();
        models.dedup();
        assert_eq!(models.len(), 16);
    }

    #[test]
    fn index_complete_model() {
        let model = Model::with_size(4);
        assert_eq!(model[1], false);
    }
}
