use super::proposition_tree::*;

impl PropositionTree {
    ///Returns an equivalent proposition in Negative Normal Form, which is a form containing only
    ///NOT, AND, OR operations.
    pub fn to_nnf(self) -> PropositionTree {
        match self {
            PropositionTree::Literal(_) => self,

            //Rules for evaluating not : not(true) -> false, not(false) -> true, not(not(p)) -> p,
            //not(a or b) -> not a and not b, not(a and b) -> not a or not b
            PropositionTree::Not(p) => {
                let p = p.to_nnf();
                match p {
                    PropositionTree::Literal(_) => PropositionTree::not(p),
                    PropositionTree::Not(p) => *p,
                    PropositionTree::BinOp(BinOp::And, p1, p2) => PropositionTree::or(
                        PropositionTree::not(*p1).to_nnf(),
                        PropositionTree::not(*p2).to_nnf(),
                    ),
                    PropositionTree::BinOp(BinOp::Or, p1, p2) => PropositionTree::and(
                        PropositionTree::not(*p1).to_nnf(),
                        PropositionTree::not(*p2).to_nnf(),
                    ),
                    _ => unreachable!(),
                }
            }

            //AND and OR simply convert their operands
            PropositionTree::BinOp(BinOp::And, p1, p2) => {
                PropositionTree::and(p1.to_nnf(), p2.to_nnf())
            }
            PropositionTree::BinOp(BinOp::Or, p1, p2) => {
                PropositionTree::or(p1.to_nnf(), p2.to_nnf())
            }

            //XOR must first convert to a xor b -> (a and not b) or (b and not a)
            PropositionTree::BinOp(BinOp::Xor, p1, p2) => {
                let (ok_p1, ok_p2) = (p1.clone().to_nnf(), p2.clone().to_nnf());
                let (not_p1, not_p2) = (
                    PropositionTree::not(*p1).to_nnf(),
                    PropositionTree::not(*p2).to_nnf(),
                );
                PropositionTree::or(
                    PropositionTree::and(ok_p1, ok_p2),
                    PropositionTree::and(not_p1, not_p2),
                )
            }

            //IMPLIES first converts to a => b -> not a or b
            PropositionTree::BinOp(BinOp::Implication, p1, p2) => {
                PropositionTree::or(PropositionTree::not(*p1).to_nnf(), p2.to_nnf())
            }

            //EQUIVALENCE first converts to a <=> b -> (a => b) and (b => a)
            PropositionTree::BinOp(BinOp::Equivalence, p1, p2) => {
                let imp1 = PropositionTree::implication(*p1.clone(), *p2.clone()).to_nnf();
                let imp2 = PropositionTree::implication(*p2, *p1).to_nnf();
                PropositionTree::and(imp1, imp2)
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use log::info;

    fn init() {
        let _ = env_logger::builder().is_test(true).try_init();
    }

    #[test]
    fn nnf_conversion_equivalence() {
        init();
        let prop = PropositionTree::implication(
            PropositionTree::or(
                PropositionTree::literal(1),
                PropositionTree::not(PropositionTree::and(
                    PropositionTree::literal(2),
                    PropositionTree::literal(3),
                )),
            ),
            PropositionTree::not(PropositionTree::equivalence(
                PropositionTree::literal(1),
                PropositionTree::literal(2),
            )),
        );
        let nnf = prop.clone().to_nnf();
        info!("NNF = {:#?}", nnf);
        assert!(equivalent(prop, nnf));
        //        assert!(false);
    }
}
