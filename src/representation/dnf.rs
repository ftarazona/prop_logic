use super::proposition_tree::*;

impl PropositionTree {
    //Recursive function to convert a proposition in DNF form.
    //The proposition is assumed to be in NNF.
    fn to_dnf_aux(self) -> PropositionTree {
        match self {
            PropositionTree::Literal(_) => self,
            PropositionTree::Not(_) => self,

            PropositionTree::BinOp(BinOp::Or, p1, p2) => {
                let (p1, p2) = (p1.to_dnf_aux(), p2.to_dnf_aux());
                PropositionTree::or(p1, p2)
            }

            //Develop an AND operation.
            PropositionTree::BinOp(BinOp::And, p1, p2) => match (*p1.clone(), *p2.clone()) {
                //Case 1 : double distributivity
                (
                    PropositionTree::BinOp(BinOp::Or, p1, p2),
                    PropositionTree::BinOp(BinOp::Or, p3, p4),
                ) => PropositionTree::or(
                    PropositionTree::or(
                        PropositionTree::or(
                            PropositionTree::and(*p2.clone(), *p4.clone()).to_dnf_aux(),
                            PropositionTree::and(*p1.clone(), *p4).to_dnf_aux(),
                        ),
                        PropositionTree::and(*p2, *p3.clone()).to_dnf_aux(),
                    ),
                    PropositionTree::and(*p1, *p3).to_dnf_aux(),
                ),

                //Case 2 : Distribute to the left
                (PropositionTree::BinOp(BinOp::Or, p1, p2), p3) => PropositionTree::or(
                    PropositionTree::and(*p1, p3.clone()).to_dnf_aux(),
                    PropositionTree::and(*p2, p3).to_dnf_aux(),
                ),

                //Case 3 : Distribute to the right
                (p1, PropositionTree::BinOp(BinOp::Or, p2, p3)) => PropositionTree::or(
                    PropositionTree::and(p1.clone(), *p2).to_dnf_aux(),
                    PropositionTree::and(p1, *p3).to_dnf_aux(),
                ),

                //Default case : as NNF is assumed, the operands are in fact literals.
                _ => PropositionTree::and(*p1, *p2),
            },

            _ => unreachable!(), //Assumed NNF
        }
    }

    ///Converts a logical proposition to a disjunction of conjunctions of literals.
    ///Consumes the object.
    pub fn to_dnf(self) -> PropositionTree {
        let p = self.to_nnf();
        p.to_dnf_aux()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use log::info;

    fn init() {
        let _ = env_logger::builder().is_test(true).try_init();
    }

    #[test]
    fn dnf_conversion_equivalence() {
        init();
        let prop = PropositionTree::implication(
            PropositionTree::or(
                PropositionTree::literal(1),
                PropositionTree::not(PropositionTree::and(
                    PropositionTree::literal(2),
                    PropositionTree::literal(3),
                )),
            ),
            PropositionTree::equivalence(PropositionTree::literal(1), PropositionTree::literal(2)),
        );
        let nnf = prop.clone().to_nnf();
        info!("NNF = \n{:#?}", nnf);
        let dnf = prop.clone().to_dnf();
        info!("DNF = \n{:#?}", dnf);
        assert!(equivalent(prop, dnf));
        //assert!(false);
    }
}
