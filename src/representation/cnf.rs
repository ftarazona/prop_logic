use super::cnf_clause::*;
use super::proposition_tree::*;
use super::*;

///A CNF is a way of representing a logical proposition as a conjunction of disjunctions of
///literals.
///CNF has the advantage of being easy to evaluate iteratively and is used by many solvers.
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct CNF {
    clauses: Vec<CNFClause>,
}

impl Default for CNF {
    fn default() -> Self {
        Self::new()
    }
}

impl CNF {
    ///Returns a new empty CNF
    pub fn new() -> CNF {
        CNF {
            clauses: Vec::new(),
        }
    }

    pub fn new_with_clauses(clauses: Vec<CNFClause>) -> CNF {
        CNF { clauses }
    }
    ///Adds a clause to the CNF
    pub fn add_clause(&mut self, cl: CNFClause) {
        self.clauses.push(cl);
    }

    ///Returns the vector of all clauses
    pub fn get_clauses(&self) -> Vec<CNFClause> {
        self.clauses.clone()
    }

    ///Returns the number of clauses
    pub fn len(&self) -> usize {
        self.clauses.len()
    }

    pub fn is_empty(&self) -> bool {
        self.clauses.is_empty()
    }

    ///Returns the list of clauses in the DIMACS format
    pub fn to_dimacs_string(&self) -> String {
        self.clauses.iter().fold(
            format!("p cnf {} {}\n", 50, self.clauses.len()),
            |acc, clause| acc + &clause.to_dimacs_string() + "\n",
        )
    }

    ///Performs the walksat algorithm to determine if the CNF is satisfiable.
    ///Please note the walksat can give false negatives as it is probabilistic.
    pub fn walksat(&self, p: f32, max_flips: usize) -> Option<Model> {
        use crate::solver::walksat;
        walksat::walksat(self, p, max_flips)
    }

    ///Performs the dpll algorithm to determine whether a CNF is satisfiable or not. If it, returns
    ///an example interpretation.
    pub fn dpll(&self) -> Option<Model> {
        use crate::solver::dpll;
        dpll::dpll(self)
    }

    ///Performs the cdcl algorithm to determine whether a CNF is satisfiable or not. If it, returns
    ///an example interpretation.
    pub fn cdcll(&self) -> Option<Model> {
        use crate::solver::cdcl;
        cdcl::cdcl(self)
    }

    ///Returns true if the CNF always evaluates to false, i.e. at least one clause is constant
    ///False.
    pub fn is_false(&self) -> bool {
        self.clauses.iter().any(|cl| cl.is_false())
    }

    ///Returns true if te CNF always evaluates to true, i.e. all of its clauses are constant True.
    pub fn is_true(&self) -> bool {
        self.clauses.iter().all(|cl| cl.is_true())
    }

    fn tseytin_transform_aux(
        prop: &PropositionTree,
        cnf: &mut CNF,
        current_var_index: usize,
    ) -> usize {
        match prop {
            PropositionTree::Literal(lit) => lit.size(),
            PropositionTree::Not(p) => {
                //To assure id unicity through the proposition tree,
                //take the id of the child + 1. If the child is a literal, we need to
                //insure the id of the NOT will not be in conflict with another literal.
                let id_child = CNF::tseytin_transform_aux(p, cnf, current_var_index);
                let id_self = std::cmp::max(id_child + 1, current_var_index);
                cnf.clauses.push(CNFClause::new_with_literals(&[
                    id_child as isize,
                    id_self as isize,
                ]));
                cnf.clauses.push(CNFClause::new_with_literals(&[
                    -(id_child as isize),
                    -(id_self as isize),
                ]));
                id_self as usize
            }
            PropositionTree::BinOp(op, p1, p2) => {
                //To assure id unicity through the proposition tree,
                //take the maximum value of both children and the current_var_index.
                //Also need to insure both sides won't be in conflict. Then we re-set
                //urrent_var_index to
                let id_child1 = CNF::tseytin_transform_aux(p1, cnf, current_var_index);
                let next_var_index = std::cmp::max(id_child1 + 1, current_var_index);
                let id_child2 = CNF::tseytin_transform_aux(p2, cnf, next_var_index);
                let id_self = std::cmp::max(id_child2 + 1, next_var_index);
                match op {
                    BinOp::And => {
                        cnf.clauses.push(CNFClause::new_with_literals(&[
                            -(id_child1 as isize),
                            -(id_child2 as isize),
                            id_self as isize,
                        ]));
                        cnf.clauses.push(CNFClause::new_with_literals(&[
                            id_child1 as isize,
                            -(id_self as isize),
                        ]));
                        cnf.clauses.push(CNFClause::new_with_literals(&[
                            id_child2 as isize,
                            -(id_self as isize),
                        ]));
                    }
                    BinOp::Or => {
                        cnf.clauses.push(CNFClause::new_with_literals(&[
                            id_child1 as isize,
                            id_child2 as isize,
                            -(id_self as isize),
                        ]));
                        cnf.clauses.push(CNFClause::new_with_literals(&[
                            -(id_child1 as isize),
                            id_self as isize,
                        ]));
                        cnf.clauses.push(CNFClause::new_with_literals(&[
                            -(id_child2 as isize),
                            id_self as isize,
                        ]));
                    }
                    _ => unreachable!(),
                };
                id_self
            }
        }
    }

    ///Returns a CNF equivalent to the given proposition
    ///Uses a Tseytin transform
    pub fn tseytin_transform(prop: PropositionTree) -> CNF {
        let mut cnf = CNF::new();
        let current_var_index = prop.max_literal() + 1;
        let _ = CNF::tseytin_transform_aux(&prop.to_nnf(), &mut cnf, current_var_index);
        cnf
    }
}

impl Proposition for CNF {
    ///Evaluates a CNF by iteratively checking all its clauses.
    ///An empty CNF evaluates to true.
    fn eval(&self, m: &Model) -> bool {
        self.clauses.iter().all(|clause| clause.eval(m))
    }

    fn partial_eval(&self, m: &PartialModel) -> Option<bool> {
        if self
            .clauses
            .iter()
            .any(|clause| clause.partial_eval(m).is_none())
        {
            None
        } else if self
            .clauses
            .iter()
            .all(|clause| clause.partial_eval(m).unwrap_or(false))
        {
            Some(true)
        } else {
            Some(false)
        }
    }

    fn size(&self) -> usize {
        let max_clause = self.clauses.iter().max_by_key(|cl| cl.size());
        match max_clause {
            Some(clause) => clause.size(),
            None => 0,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::super::*;
    use super::*;
    /********** CNF evaluation tests ************/
    #[test]
    fn satisfied_cnf() {
        let i = Model::from(&[false, true, true, false]);

        let cnf = CNF::new_with_clauses(vec![
            CNFClause::new_with_literals(&[-1, 2]),
            CNFClause::new_with_literals(&[-2, -3, -4]),
        ]);

        assert!(cnf.eval(&i));
    }

    #[test]
    fn unsatisfied_cnf() {
        let i = Model::from(&[false, true, true, false]);

        let cnf = CNF::new_with_clauses(vec![
            CNFClause::new_with_literals(&[-1, 2]),
            CNFClause::new_with_literals(&[-2, -3, 4]),
        ]);

        assert!(!cnf.eval(&i));
    }

    #[test]
    fn cnf_to_dimacs() {
        let cnf = CNF::new_with_clauses(vec![
            CNFClause::new_with_literals(&[-1, 2]),
            CNFClause::new_with_literals(&[-2, -3, 4]),
        ]);

        assert_eq!(
            cnf.to_dimacs_string(),
            String::from(
                "\
p cnf 50 2
-1 2 
-2 -3 4 
"
            )
        );
    }
}
