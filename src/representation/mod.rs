pub mod cnf;
pub mod cnf_clause;
pub mod dnf;
pub mod literal;
pub mod model;
pub mod nnf;
pub mod proposition_tree;

///Describes what a proposition should propose as an interface
use model::*;
pub trait Proposition {
    ///Evaluates the proposition given a model
    fn eval(&self, m: &Model) -> bool;

    ///Evaluates the proposition given a partial model
    fn partial_eval(&self, m: &PartialModel) -> Option<bool>;

    ///Returns the number of variables necessary to evaluate the proposition
    fn size(&self) -> usize;
}
