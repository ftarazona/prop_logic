use super::model::*;
use super::*;

///Literal structure represents a literal, which may be a constant or a variable which will
///evaluate to true depending on the value given by the interpretation.
#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub enum Literal {
    True,
    False,
    Variable(isize),
}

impl Literal {
    ///Creates a constant True
    pub fn new_true() -> Literal {
        Literal::True
    }

    ///Creates a constant False
    pub fn new_false() -> Literal {
        Literal::False
    }

    ///Creates a new variable given a sign-encoded variable, which is minus sign indicates the
    ///variable must be false so that the literal evaluates to true.
    pub fn new_variable(var: isize) -> Literal {
        assert!(var != 0, "Null literal is not allowed");
        Literal::Variable(var)
    }
}

impl Proposition for Literal {
    fn eval(&self, m: &Model) -> bool {
        match self {
            Literal::True => true,
            Literal::False => false,
            Literal::Variable(var) => m.eval(*var),
        }
    }

    fn partial_eval(&self, m: &PartialModel) -> Option<bool> {
        match self {
            Literal::True => Some(true),
            Literal::False => Some(false),
            Literal::Variable(var) => m.partial_eval(*var),
        }
    }

    fn size(&self) -> usize {
        match self {
            Literal::Variable(var) => var.abs() as usize,
            _ => 0,
        }
    }
}

impl ToString for Literal {
    fn to_string(&self) -> String {
        match self {
            Literal::Variable(var) => var.to_string(),
            _ => String::new(),
        }
    }
}

/*****************************************************************************************/
/*************************************** UNIT TESTS **************************************/
/*****************************************************************************************/
#[cfg(test)]
mod tests {
    use super::*;

    /*** EVALUATION TESTS ***/
    #[test]
    fn literal_complete_evaluation() {
        let m = Model::from(&[false, true]);

        let l = Literal::new_true();
        assert!(l.eval(&m));

        let l = Literal::new_false();
        assert!(!l.eval(&m));

        let l = Literal::new_variable(-1);
        assert!(l.eval(&m));
        let l = Literal::new_variable(-2);
        assert!(!l.eval(&m));
        let l = Literal::new_variable(1);
        assert!(!l.eval(&m));
        let l = Literal::new_variable(2);
        assert!(l.eval(&m));
    }

    #[test]
    #[should_panic]
    fn literal_complete_evaluation_null_literal() {
        let _m = Model::with_size(2);

        let _l = Literal::new_variable(0);
    }

    #[test]
    #[should_panic]
    fn literal_complete_evaluation_invalid_literal() {
        let m = Model::with_size(2);

        let l = Literal::new_variable(3);
        let _ = l.eval(&m);
    }

    #[test]
    fn literal_partial_evaluation() {
        let mut m = PartialModel::with_size(2);
        m.set(-1);

        let l = Literal::new_true();
        assert!(l.partial_eval(&m).unwrap());

        let l = Literal::new_false();
        assert!(!l.partial_eval(&m).unwrap());

        let l = Literal::new_variable(-1);
        assert!(l.partial_eval(&m).unwrap());
        let l = Literal::new_variable(-2);
        assert!(l.partial_eval(&m).is_none());
        let l = Literal::new_variable(1);
        assert!(!l.partial_eval(&m).unwrap());
        let l = Literal::new_variable(2);
        assert!(l.partial_eval(&m).is_none());
    }

    #[test]
    #[should_panic]
    fn literal_partial_evaluation_null_literal() {
        let _m = PartialModel::with_size(2);

        let _l = Literal::new_variable(0);
    }

    #[test]
    #[should_panic]
    fn literal_partial_evaluation_invalid_literal() {
        let m = PartialModel::with_size(2);

        let l = Literal::new_variable(3);
        let _ = l.partial_eval(&m);
    }

    /*** SIZE OF A LITERAL ***/
    #[test]
    fn literal_size() {
        let l = Literal::new_true();
        assert!(l.size() == 0);

        let l = Literal::new_false();
        assert!(l.size() == 0);

        let l = Literal::new_variable(4);
        assert!(l.size() == 4);

        let l = Literal::new_variable(-5);
        assert!(l.size() == 5);
    }

    /*** LITERAL TO STRING CONVERSION ***/
    #[test]
    fn literal_to_string() {
        let l = Literal::new_true();
        assert!(&l.to_string() == "");

        let l = Literal::new_false();
        assert!(&l.to_string() == "");

        let l = Literal::new_variable(2);
        assert!(&l.to_string() == "2");

        let l = Literal::new_variable(-6);
        assert!(&l.to_string() == "-6");
    }
}
