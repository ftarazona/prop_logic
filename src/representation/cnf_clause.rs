use super::literal::*;
use super::*;

///A CNF clause consists in a disjunction of literals.
///The literals are represented as a list.
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct CNFClause {
    literals: Vec<Literal>,
}

impl Default for CNFClause {
    fn default() -> Self {
        Self::new()
    }
}

impl CNFClause {
    ///Returns a new empty clause.
    pub fn new() -> CNFClause {
        CNFClause {
            literals: Vec::new(),
        }
    }

    ///Returns a new clause containing the given literals
    pub fn new_with_literals(literals: &[isize]) -> CNFClause {
        CNFClause {
            literals: literals
                .iter()
                .map(|lit| Literal::new_variable(*lit))
                .collect(),
        }
    }

    ///Adds a literal to the clause
    pub fn add_literal(&mut self, lit: Literal) {
        self.literals.push(lit);
    }

    ///Returns the list of literals in clause. The returned list is owned.
    pub fn get_literals(&self) -> Vec<Literal> {
        self.literals.clone()
    }

    pub fn as_variables(&self) -> Vec<isize> {
        let mut vars = Vec::new();
        for literal in self.literals.iter() {
            if let Literal::Variable(var) = literal {
                vars.push(*var)
            }
        }
        vars.sort();
        vars.dedup();
        vars
    }

    ///Returns the list of literals in the DIMACS format
    pub fn to_dimacs_string(&self) -> String {
        self.literals
            .iter()
            .fold(String::new(), |acc, lit| acc + &(lit.to_string() + " "))
    }

    ///A clause is always false if it is not empty and each of its literals are constant Falses
    pub fn is_false(&self) -> bool {
        !self.literals.is_empty() && self.literals.iter().all(|lit| *lit == Literal::False)
    }

    ///A clause is always true if it is empty or if at least one of its literals is constant True
    pub fn is_true(&self) -> bool {
        self.literals.is_empty() || self.literals.iter().any(|lit| *lit == Literal::True)
    }
}

impl Proposition for CNFClause {
    ///Evaluates the clause regarding a given interpretation.
    ///Note the empty clauses evaluates to true.
    fn eval(&self, m: &Model) -> bool {
        if self.literals.is_empty() {
            true
        } else {
            self.literals.iter().for_each(|lit| {
                if lit.size() > m.size() {
                    panic!("Invalid literal");
                }
            });
            self.literals.iter().any(|lit| lit.eval(m))
        }
    }

    ///Evaluates the clause regarding a given interpretation.
    ///Note the empty clauses evaluates to true.
    fn partial_eval(&self, m: &PartialModel) -> Option<bool> {
        if self.literals.is_empty() {
            Some(true)
        } else {
            self.literals.iter().for_each(|lit| {
                if lit.size() > m.size() {
                    panic!("Invalid literal");
                }
            });
            if self
                .literals
                .iter()
                .any(|lit| lit.partial_eval(m).is_none())
            {
                None
            } else if self
                .literals
                .iter()
                .any(|lit| lit.partial_eval(m).unwrap_or(false))
            {
                Some(true)
            } else {
                Some(false)
            }
        }
    }

    ///Returns the max value of literal referenced in the clause
    fn size(&self) -> usize {
        self.literals
            .iter()
            .map(|lit| lit.size())
            .max()
            .unwrap_or(0)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    /********** Clause evaluation tests ***********/
    #[test]
    fn evaluate_clause() {
        let m = Model::from(&[false, true, true, false]);

        let mut cl = CNFClause::new_with_literals(&[1, -2]);
        assert!(!cl.eval(&m));

        cl.add_literal(Literal::new_variable(3));
        assert!(cl.eval(&m));

        cl.add_literal(Literal::new_variable(4));
        assert!(cl.eval(&m));
    }

    #[test]
    #[should_panic]
    fn evaluate_clause_with_out_of_bounds_literal() {
        let m = Model::from(&[false, true, true, false]);

        let cl = CNFClause::new_with_literals(&[-1, 2, 10]);

        let _res = cl.eval(&m);
    }

    #[test]
    #[should_panic]
    fn evaluate_clause_with_null_literal() {
        let m = Model::from(&[false, true, true, false]);

        let cl = CNFClause::new_with_literals(&[-1, 2, 0]);

        let _res = cl.eval(&m);
    }

    #[test]
    fn clause_to_dimacs() {
        let cl = CNFClause::new_with_literals(&[-1, 2, -3]);
        assert_eq!(cl.to_dimacs_string(), String::from("-1 2 -3 "));

        let mut cl = CNFClause::new_with_literals(&[-1, 2, -3]);
        cl.add_literal(Literal::True);
        assert_eq!(cl.to_dimacs_string(), String::from("-1 2 -3  "));
    }
}
