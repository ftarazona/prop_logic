/* This binary reads a graph from a file and colors with a given number of colors
 * so that :
 *  - each node has one and only one color
 *  - two adjacent nodes do not have the same color
 */

use std::env;

use prop_logic::representation::cnf::*;
use prop_logic::representation::cnf_clause::*;
use prop_logic::representation::literal::*;
use prop_logic::representation::model::*;
use prop_logic::solver::cdcl::cdcl;

fn main() {
    let mut args = env::args();
    args.next();
    let filename = args
        .next()
        .expect("Usage : k_graph_coloring <graph file> k");
    let k = args
        .next()
        .expect("Usage : k_graph_coloring <graph file> k")
        .parse::<u32>()
        .expect("Non readable number of colors");
    let edges = read_graph_from_file(&filename);
    let n = edges
        .iter()
        .map(|(n1, n2)| std::cmp::max(*n1, *n2))
        .max()
        .unwrap()
        + 1;
    let cnf = graph_to_cnf(k, n, &edges);
    match cdcl(&cnf) {
        Some(interpretation) => println!("{:#?}", interpretation_to_colors(k, n, &interpretation)),
        None => println!("No solution found !"),
    }
}

use std::fs;
fn read_graph_from_file(filename: &str) -> Vec<(u32, u32)> {
    let content = fs::read_to_string(filename).expect("Error occured while reading input file");

    let integer_pairs = content
        .as_str()
        .split_ascii_whitespace()
        .collect::<Vec<&str>>();

    integer_pairs
        .chunks_exact(2)
        .map(|integers| {
            (
                integers[0].parse::<u32>().expect("Mal formatted node"),
                integers[1].parse::<u32>().expect("Mal formatted node"),
            )
        })
        .collect()
}

/* format of the cnf : p_vi is the index v * k + i */
fn graph_to_cnf(k: u32, n: u32, edges: &[(u32, u32)]) -> CNF {
    let mut cnf = CNF::new();

    for v in 0..n {
        let mut clause = CNFClause::new();
        for i in 0..k {
            clause.add_literal(Literal::new_variable((v * k + i) as isize + 1));
        }
        cnf.add_clause(clause);
    }

    for v in 0..n {
        for i in 0..k {
            for j in (i + 1)..k {
                cnf.add_clause(CNFClause::new_with_literals(&[
                    -((v * k + i) as isize) - 1,
                    -((v * k + j) as isize) - 1,
                ]));
            }
        }
    }

    for (v, w) in edges {
        for i in 0..k {
            cnf.add_clause(CNFClause::new_with_literals(&[
                -((v * k + i) as isize) - 1,
                -((w * k + i) as isize) - 1,
            ]));
        }
    }

    cnf
}

fn interpretation_to_colors(k: u32, n: u32, m: &Model) -> Vec<u32> {
    (0..n)
        .map(|v| {
            (0..k)
                .find_map(|i| {
                    if m[(v * k + i) as usize + 1] {
                        Some(i)
                    } else {
                        None
                    }
                })
                .unwrap()
        })
        .collect()
}
