/* This binary reads a sudoku from and then solves it
 */

use std::env;

use prop_logic::representation::cnf::*;
use prop_logic::representation::cnf_clause::*;
use prop_logic::representation::literal::*;
use prop_logic::representation::model::*;
use prop_logic::representation::*;
use prop_logic::solver::cdcl::cdcl;

fn main() {
    let mut args = env::args();
    args.next();
    let filename = args
        .next()
        .expect("Usage : k_graph_coloring <graph file> k");

    let grid = read_grid_from_file(&filename);
    assert_eq!(grid.len(), 81);

    let cnf = sudoku_to_cnf(&grid);

    let sol = cdcl(&cnf).expect("Possibly unsolvable");
    //    println!("{:#?}", sol);
    assert!(cnf.eval(&sol));
    let solved_grid = interpretation_to_grid(&sol);

    println!("{}", grid_to_string(&solved_grid));
}

use std::fs;
fn read_grid_from_file(filename: &str) -> Vec<Option<u32>> {
    let content = fs::read_to_string(filename).expect("Error occured while reading input file");

    content
        .as_str()
        .split_ascii_whitespace()
        .map(|n| n.parse::<u32>().ok())
        .collect()
}

/* format of the cnf : p_ni is the index n * 9 + i */
fn sudoku_to_cnf(grid: &[Option<u32>]) -> CNF {
    let mut cnf = CNF::new();

    for (n, i) in grid.iter().enumerate() {
        if let Some(i) = i {
            cnf.add_clause(CNFClause::new_with_literals(&[(n * 9 + (*i - 1) as usize)
                as isize
                + 1]));
        }
    }

    //Each row has both numbers
    for row in 0..9 {
        for i in 0..9 {
            let mut clause = CNFClause::new();
            for col in 0..9 {
                clause.add_literal(Literal::Variable((81 * row + 9 * col + i) as isize + 1));
            }
            cnf.add_clause(clause);
        }
    }

    //Each column has both numbers
    for col in 0..9 {
        for i in 0..9 {
            let mut clause = CNFClause::new();
            for row in 0..9 {
                clause.add_literal(Literal::Variable((81 * row + 9 * col + i) as isize + 1));
            }
            cnf.add_clause(clause);
        }
    }

    //Each region has both numbers
    for h in 0..3 {
        for k in 0..3 {
            for i in 0..9 {
                let mut clause = CNFClause::new();
                for row in 0..3 {
                    for col in 0..3 {
                        clause.add_literal(Literal::Variable(
                            ((h * 3 + row) * 81 + (k * 3 + col) * 9 + i) as isize + 1,
                        ));
                    }
                }
                cnf.add_clause(clause);
            }
        }
    }

    //Each cell can contain ONE AND ONLY ONE number
    for c in 0..81 {
        for i in 0..9 {
            for j in (i + 1)..9 {
                cnf.add_clause(CNFClause::new_with_literals(&[
                    -((c * 9 + i) as isize) - 1,
                    -((c * 9 + j) as isize) - 1,
                ]));
            }
        }
    }

    //A deduced constraint is that each row, column or region can contain each number at most once.
    //This allows for two-literal clauses which will allow for more propagation
    for i in 0..9   {
        for row in 0..9 {
            for col1 in 0..9  {
                for col2 in (col1+1)..9 {
                    cnf.add_clause(CNFClause::new_with_literals(&[
                        -((81 * row + 9 * col1 + i) as isize + 1),
                        -((81 * row + 9 * col2 + i) as isize + 1),
                    ]));
                }
            }
        }

        for col in 0..9 {
            for row1 in 0..9  {
                for row2 in (row1+1)..9 {
                    cnf.add_clause(CNFClause::new_with_literals(&[
                        -((81 * row1 + 9 * col + i) as isize + 1),
                        -((81 * row2 + 9 * col + i) as isize + 1),
                    ]));
                }
            }
        }

        for reg in 0..9 {
            let h = reg / 3;
            let k = reg % 3;
            for c1 in 0..9  {
                for c2 in (c1+1)..9 {
                    let row1 = c1 / 3;
                    let col1 = c1 % 3;
                    let row2 = c2 / 3;
                    let col2 = c2 % 3;
                    cnf.add_clause(CNFClause::new_with_literals(&[
                        -(((h * 3 + row1) * 81 + (k * 3 + col1) * 9 + i) as isize + 1),
                        -(((h * 3 + row2) * 81 + (k * 3 + col2) * 9 + i) as isize + 1),
                    ]));
                }
            }
        }
    }


    cnf
}

fn interpretation_to_grid(int: &Model) -> Vec<u32> {
    (0..81)
        .map(|n| {
            (0..9)
                .find_map(|i| {
                    if int[(n * 9 + i) as usize + 1] {
                        Some(i + 1)
                    } else {
                        None
                    }
                })
                .unwrap()
        })
        .collect()
}

fn grid_to_string(grid: &[u32]) -> String {
    let mut s = String::new();
    let mut n = 0;
    for _block_line in 0..3 {
        for _line in 0..3 {
            for _block_row in 0..3 {
                for _row in 0..3 {
                    s.push_str(grid[n].to_string().as_str());
                    s.push(' ');
                    n += 1;
                }
                s.push(' ');
            }
            s.push('\n');
        }
        s.push('\n');
    }
    s
}
