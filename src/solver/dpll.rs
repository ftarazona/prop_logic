use crate::representation::cnf::*;
use crate::representation::model::*;
use crate::representation::Proposition;

use super::bcp::*;

///DPLL is a deterministic algorithm for finding out, if it exists, an interpretation satisfaying a
///given proposition in CNF form.
///
///The main idea of CNF is to deduce as many literals as possible with unit clauses (clauses with a
///unique literal). When unit propagation becomes unable to deduce any other literal, we choose a
///literal which could not be deduced and we try to find a satifiable interpretation enforcing the
///value of this literal.
pub fn dpll(cnf: &CNF) -> Option<Model> {
    let cnf_base = cnf;
    let mut trail: Vec<TrailUnit> = Vec::new();
    let mut backups: Vec<(EvolutiveCNF, PartialModel)> = Vec::new();

    backups.push((
        EvolutiveCNF::from_cnf(cnf),
        PartialModel::with_size(cnf.size()),
    ));

    while let Some((mut cnf, mut model)) = backups.pop() {
        //Repeat unit propagation / choose until a conflict is met
        if let Some((id, TrailUnit::Decision(var))) =
            trail
                .iter()
                .enumerate()
                .rev()
                .find_map(|(id, trail_unit)| match trail_unit {
                    TrailUnit::Decision(var) if *var > 0 => Some((id, trail_unit)),
                    _ => None,
                })
        {
            let var = var.clone();
            trail = trail[0..id].to_vec();
            trail.append(&mut cnf.choose(&mut model, -var));
        }
        loop {
            trail.append(&mut cnf.bcp_with_trail(&mut model));
            if cnf_base.partial_eval(&model) == Some(true) {
                return Some(model.to_model());
            }
            if let Some(&TrailUnit::Conflict(_, _)) = trail.last() {
                break;
            } else {
                backups.push((cnf.clone(), model.clone()));
                let chosen_var = model.choose_next(true).expect("Could not choose...");
                trail.append(&mut cnf.choose(&mut model, chosen_var as isize));
            }
        }
    }

    None
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::representation::cnf_clause::*;

    #[test]
    fn find_satisfiable_model_with_dpll() {
        let cnf = CNF::new_with_clauses(vec![
            CNFClause::new_with_literals(&[-1, -2, -6]),
            CNFClause::new_with_literals(&[-1, -2, 6]),
            CNFClause::new_with_literals(&[-1, -3, -5]),
        ]);

        assert!(cnf.eval(&dpll(&cnf).unwrap()));
    }

    #[test]
    fn find_unsatisfiable_with_dpll() {
        let cnf = CNF::new_with_clauses(vec![
            CNFClause::new_with_literals(&[1, 2]),
            CNFClause::new_with_literals(&[-1, -2]),
            CNFClause::new_with_literals(&[-1, 2]),
            CNFClause::new_with_literals(&[1, -2]),
        ]);

        assert!(dpll(&cnf).is_none());
    }
}
