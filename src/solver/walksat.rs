use crate::representation::cnf::*;
use crate::representation::model::*;
use crate::representation::*;

///Walksat algorithm attempts to find a satisfying interpretation for a given proposition in CNF.
///The approach is probabilistic : we try to flip random literals to search. Sometimes we may also
///try to flip a literal that will maximize the number of satisfied clauses.
pub fn walksat(cnf: &CNF, p: f32, max_flips: usize) -> Option<Model> {
    use rand::Rng;

    let mut model = Model::random_with_size(cnf.size());
    let mut rng = rand::thread_rng();

    for _ in 0..max_flips {
        if cnf.eval(&model) {
            return Some(model);
        } else {
            let clauses = cnf.get_clauses();
            let clause = clauses[rng.gen_range(0..clauses.len())].clone();

            if rand::random::<f32>() < p {
                //Randomly select a variable from the clause to flip in the model
                let literals = clause.get_literals();
                let literal = literals[rng.gen_range(0..literals.len())].size();
                model.flip(literal);
            } else {
                let clause = clause.get_literals();
                let literal = clause
                    .iter()
                    .zip(clause.iter().map(|literal| {
                        model.flip(literal.size());
                        let count = clauses.iter().fold(0, |acc, cl| {
                            acc + {
                                if cl.eval(&model) {
                                    1
                                } else {
                                    0
                                }
                            }
                        });
                        model.flip(literal.size());
                        count
                    }))
                    .max_by_key(|(_, count)| *count)
                    .unwrap()
                    .0;
                model.flip(literal.size());
            }
        }
    }

    None
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::representation::cnf_clause::*;

    #[test]
    fn find_satisfiable_model_with_walksat() {
        let cnf = CNF::new_with_clauses(vec![
            CNFClause::new_with_literals(&[-1, 2, 4]),
            CNFClause::new_with_literals(&[1, -2, 6]),
            CNFClause::new_with_literals(&[1, -3, 5]),
        ]);

        assert!(walksat(&cnf, 0.5, 30).is_some());
    }

    #[test]
    fn find_unsatisfiable_with_walksat() {
        let cnf = CNF::new_with_clauses(vec![
            CNFClause::new_with_literals(&[1, 2]),
            CNFClause::new_with_literals(&[-1, -2]),
            CNFClause::new_with_literals(&[-1, 2]),
            CNFClause::new_with_literals(&[1, -2]),
        ]);

        assert!(walksat(&cnf, 0.5, 30).is_none());
    }
}
