use crate::representation::cnf::*;
use crate::representation::model::*;

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum TrailUnit {
    Propagation(isize, Vec<isize>),
    Decision(isize),
    Conflict(isize, Vec<isize>),
}

#[derive(Clone, Debug)]
pub struct EvolutiveCNF {
    clauses: Vec<(Vec<isize>, Vec<isize>)>,
}

impl EvolutiveCNF {
    pub fn from_cnf(cnf: &CNF) -> EvolutiveCNF {
        EvolutiveCNF {
            clauses: cnf
                .get_clauses()
                .iter()
                .map(|clause| {
                    let vars = clause.as_variables();
                    (vars.clone(), vars)
                })
                .filter(|(vars, _)| vars.len() > 0)
                .collect(),
        }
    }

    pub fn add_clause(&mut self, clause: Vec<isize>) {
        self.clauses.push((clause.clone(), clause));
    }

    pub fn bcp_with_trail(&mut self, m: &mut PartialModel) -> Vec<TrailUnit> {
        //To keep track of the clauses to evaluate we use a vector with tuples :
        // (current_clause, original_clause)
        //The clauses are described as vectors of isize.
        //The current clauses are modified at each iteration to remove the deduced variable
        //The original clauses are used to trace the trail

        //Put least long clauses at the end
        self.clauses.sort_by_key(|(vars, _)| -(vars.len() as isize));

        let mut trail = Vec::new();

        while let Some((current_clause, original_clause)) = self.clauses.pop() {
            if current_clause.len() > 1 {
                self.clauses.push((current_clause, original_clause));
                break;
            } else {
                let deduced = current_clause[0];
                m.set(deduced);
                trail.push(TrailUnit::Propagation(deduced, original_clause));
                self.clauses
                    .retain(|(current_clause, _)| current_clause.iter().all(|var| *var != deduced));

                if let Some((_, original_clause)) = self
                    .clauses
                    .iter()
                    .rev()
                    .find(|(cl, _)| cl.len() == 1 && cl[0] == -deduced)
                {
                    trail.push(TrailUnit::Conflict(deduced, original_clause.clone()));
                    break;
                }

                self.clauses
                    .iter_mut()
                    .for_each(|(current_clause, _)| current_clause.retain(|var| *var != -deduced));
                self.clauses.sort_by_key(|(vars, _)| -(vars.len() as isize));
                self.clauses
                    .retain(|(current_clause, _)| current_clause.len() > 0);
            }
        }

        trail
    }

    pub fn choose(&mut self, m: &mut PartialModel, chosen_var: isize) -> Vec<TrailUnit> {
        m.set(chosen_var);

        self.clauses.iter_mut().for_each(|(current_clause, _)| {
            current_clause.retain(|var| *var != -chosen_var);
        });
        self.clauses
            .retain(|(current_clause, _)| current_clause.iter().all(|var| *var != chosen_var));

        vec![TrailUnit::Decision(chosen_var)]
    }
}
