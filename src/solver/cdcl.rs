///Conflict Driven Clause Learning is an alternative algorithm based on DPLL for solving the SAT
///problem.
///
///The difference occurs when a conflict is found, requiring the algorithm to go back and choose
///one literal otherwise. While, in DPLL, whe switched the last literal chosen, CDCL tries to learn
///from the conflict that occured.
///
///When a conflict occurs, the analysis is as follows :
/// - We find the direct dependencies of the literal that caused conflict.
/// - A new clause forbiding them to be both in a way that led to the conflict is added to the CNF
/// - A backjump to the lowest decision level where one variable of this clause was chosen occurs
///
use crate::representation::cnf::*;
use crate::representation::model::*;
use crate::representation::Proposition;

use super::bcp::*;

fn analyze_conflict(
    trail: &Vec<(TrailUnit, usize)>,
    mut conflict_var: isize,
    mut conflict_clause: Vec<isize>,
) -> (usize, Vec<isize>) {

    conflict_clause.iter_mut().for_each(|var| *var = var.abs());
    conflict_var = conflict_var.abs();
    
    for (trail_unit, _) in trail.iter()    {
        match trail_unit    {
            TrailUnit::Propagation(var, involved_vars) if var.abs() == conflict_var =>
                conflict_clause.append(&mut involved_vars.clone()),
            _ => {},
        }
    }

    conflict_clause.iter_mut().for_each(|var| *var = var.abs());
    conflict_clause.retain(|var| *var != conflict_var);

    let mut min_level = usize::MAX;
    let mut learned_clause: Vec<isize> = Vec::new();

    for (trail_unit, decision_level) in trail.iter() {
        match trail_unit {
            TrailUnit::Propagation(var, _) if conflict_clause.contains(&var.abs()) => {
                learned_clause.push(-var);
                min_level = std::cmp::min(min_level, *decision_level);
            }
            TrailUnit::Decision(var) if conflict_clause.contains(&var.abs()) => {
                learned_clause.push(-var);
                min_level = std::cmp::min(min_level, *decision_level);
            }
            _ => {},
        }
    }

    (min_level, learned_clause)
}

///DPLL is a deterministic algorithm for finding out, if it exists, an interpretation satisfaying a
///given proposition in CNF form.
///
///The main idea of CNF is to deduce as many literals as possible with unit clauses (clauses with a
///unique literal). When unit propagation becomes unable to deduce any other literal, we choose a
///literal which could not be deduced and we try to find a satifiable interpretation enforcing the
///value of this literal.
pub fn cdcl(cnf: &CNF) -> Option<Model> {
    let cnf_base = cnf;
    let mut decision_level: usize = 0;
    let mut trail: Vec<(TrailUnit, usize)> = Vec::new();
    let mut backups: Vec<(usize, EvolutiveCNF, PartialModel)> = Vec::new();

//    backups.push((
//        0,
//        EvolutiveCNF::from_cnf(cnf),
//        PartialModel::with_size(cnf.size()),
//    ));

    let mut cnf = EvolutiveCNF::from_cnf(cnf);
    let mut model = PartialModel::with_size(cnf_base.size());

    loop    {
        loop {
            trail.append(
                &mut cnf
                    .bcp_with_trail(&mut model)
                    .into_iter()
                    .map(|tu| (tu, decision_level))
                    .collect(),
            );
            if cnf_base.partial_eval(&model) == Some(true) {
                return Some(model.to_model());
            }
            let last_trail_unit = trail.pop();
            if let Some((TrailUnit::Conflict(conflict_var, conflict_clause), _)) = last_trail_unit {
                //Analyze the conflict to find which learned clause to add
                //and which decision level to back_jump to
                if decision_level == 0   {
                    return None;
                }

                let mut learned_clause;
                (decision_level, learned_clause) =
                    analyze_conflict(&trail, conflict_var, conflict_clause);

                if decision_level == usize::MAX {
                    return None;
                }
               
                while backups.len() > decision_level + 1    {
                    let _ = backups.pop();
                }
                let trail_id;
                (trail_id, cnf, model) = backups.pop().expect("Unable to backjump");
                learned_clause.retain(|var| model[var.abs() as usize] == None);
                
                trail = trail[0..trail_id].to_vec();
                cnf.add_clause(learned_clause.clone());
                backups.iter_mut().for_each(|(_, cnf, _)| cnf.add_clause(learned_clause.clone()));
                backups.push((trail_id, cnf.clone(), model.clone()));
                break;
            } else {
                //Make a decision
                if let Some(last_trail_unit) = last_trail_unit {
                    trail.push(last_trail_unit);
                }
                backups.push((trail.len(), cnf.clone(), model.clone()));
                decision_level += 1;
                let chosen_var = model.choose_next(true).expect("Could not choose...");
                trail.append(
                    &mut cnf
                        .choose(&mut model, chosen_var as isize)
                        .into_iter()
                        .map(|tu| (tu, decision_level))
                        .collect(),
                );
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::representation::cnf_clause::*;

    #[test]
    fn find_satisfiable_model_with_cdcl() {
        let cnf = CNF::new_with_clauses(vec![
            CNFClause::new_with_literals(&[-1, -2, -6]),
            CNFClause::new_with_literals(&[-1, -2, 6]),
            CNFClause::new_with_literals(&[1, -3, 5]),
        ]);
        assert!(cnf.eval(&cdcl(&cnf).unwrap()));

        let cnf = CNF::new_with_clauses(vec![
            CNFClause::new_with_literals(&[1, 4]),
            CNFClause::new_with_literals(&[1, -3, -8]),
            CNFClause::new_with_literals(&[1, 8, 12]),
            CNFClause::new_with_literals(&[2, 11]),
            CNFClause::new_with_literals(&[-7, -3, 9]),
            CNFClause::new_with_literals(&[-7, 8, -9]),
            CNFClause::new_with_literals(&[7, 8, -10]),
            CNFClause::new_with_literals(&[7, 10, -12]),
        ]);
        assert!(cnf.eval(&cdcl(&cnf).unwrap()));
    }

    #[test]
    fn find_unsatisfiable_with_cdcl() {
        let cnf = CNF::new_with_clauses(vec![
            CNFClause::new_with_literals(&[1, 2]),
            CNFClause::new_with_literals(&[-1, -2]),
            CNFClause::new_with_literals(&[-1, 2]),
            CNFClause::new_with_literals(&[1, -2]),
        ]);

        println!("{:#?}", cdcl(&cnf));
        assert!(cdcl(&cnf).is_none());
    }
}
