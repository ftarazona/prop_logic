use crate::representation::cnf::*;
use crate::representation::cnf_clause::*;
use crate::representation::literal::*;
use log::{error, warn};
use std::fs;

///Reads a file formatted as dimacs.
///Returns the parsed CNF, the number of vaiables and of clauses
pub fn read_dimacs_file(filename: &str) -> Option<(CNF, usize, usize)> {
    let mut n_vars = 0;
    let mut n_clauses = 0;
    let mut clause_counter = 0;
    let mut cnf_declared = false;
    let mut cnf = CNF::new();

    let content = match fs::read_to_string(filename) {
        Ok(content) => content,
        Err(_) => {
            error!("read_dimacs_file: could not open file {filename}");
            return None;
        }
    };

    for line in content.lines() {
        if line.starts_with('p') {
            //Reading cnf declaration
            /* if a cnf is already declared, the file is malformatted. */
            if cnf_declared {
                warn!("read_dimacs_file: found multiple cnf declarations in {filename}");
                return None;

            /* otherwise, we read the number of variables and the number of clauses.
             * In a well formatted dimacs file, p would be followed by the token "cnf".
             * However, for simplicity sake, we here ignore it and consider the file
             * well formatted as long as there is a token between "p" and the data */
            } else {
                let mut split = line.split_ascii_whitespace();
                n_vars = match split.nth(2) {
                    //Read number of variables
                    Some(n_vars) => match n_vars.parse() {
                        Ok(n_vars) => n_vars,
                        Err(_) => return None,
                    },
                    None => return None,
                };
                n_clauses = match split.next() {
                    //Read number of clauses
                    Some(n_clauses) => match n_clauses.parse() {
                        Ok(n_clauses) => n_clauses,
                        Err(_) => return None,
                    },
                    None => return None,
                };
            }
            cnf_declared = true;
        } else if !line.starts_with('c') {
            //Reading clause declaration
            /* If no cnf was declared before, then the file is malformatted. */
            if !cnf_declared {
                warn!("read_dimacs_file: found clause declaration before cnf declaration");
                return None;
            }

            /* Otherwise, we read each clause on different lines.
             * A clause can be empty. */
            let mut clause = CNFClause::new();
            for split in line.split_ascii_whitespace() {
                match split.parse::<isize>() {
                    Ok(lit) if lit.unsigned_abs() > n_vars => {
                        warn!("read_dimacs_file: found out of bounds literal");
                        return None;
                    }
                    Ok(lit) if lit == 0 => {
                        warn!("read_dimacs_file: null literal");
                        return None;
                    }
                    Ok(lit) => clause.add_literal(Literal::new_variable(lit)),
                    Err(_) => {
                        warn!("read_dimacs_file: found invalid literal {split}");
                        return None;
                    }
                }
            }
            cnf.add_clause(clause);
            clause_counter += 1;
        }
    }

    //If the number of read clauses differs from the number of clauses declared, the file
    //is malformatted.
    if n_clauses != clause_counter {
        warn!("read_dimacs_file: file {filename} is malformatted : {n_clauses} declared, {clause_counter} found");
        return None;
    }

    Some((cnf, n_vars, n_clauses))
}

///Writes a CNF into a file in DIMACS format
pub fn write_dimacs_file(filename: &str, cnf: &CNF) -> std::io::Result<()> {
    fs::write(filename, &cnf.to_dimacs_string())
}

#[cfg(test)]
mod tests {
    use super::*;

    fn init() {
        let _ = env_logger::builder().is_test(true).try_init();
    }

    #[test]
    #[should_panic]
    fn read_dimacs_file_not_found() {
        read_dimacs_file("tests/not_found").unwrap();
    }

    #[test]
    fn read_malformatted_dimacs_files() {
        //Lacking clause number declaration
        assert!(read_dimacs_file("tests/dimacs/dimacs_malformatted1").is_none());

        //Unparsable literal
        assert!(read_dimacs_file("tests/dimacs/dimacs_malformatted2").is_none());

        //Multiple cnf declarations
        assert!(read_dimacs_file("tests/dimacs/dimacs_malformatted3").is_none());

        //Out of bounds literal
        assert!(read_dimacs_file("tests/dimacs/dimacs_malformatted4").is_none());

        //Null literal
        assert!(read_dimacs_file("tests/dimacs/dimacs_malformatted5").is_none());

        //n_clauses != clause_counter
        assert!(read_dimacs_file("tests/dimacs/dimacs_malformatted6").is_none());
    }

    #[test]
    fn read_well_formatted_dimacs_file() {
        init();
        let (cnf, n_vars, n_clauses) =
            read_dimacs_file("tests/dimacs/dimacs_well_formatted").unwrap();
        assert_eq!((cnf.len(), n_vars, n_clauses), (3, 5, 3));

        let (cnf, n_vars, n_clauses) =
            read_dimacs_file("tests/dimacs/dimacs_well_formatted2").unwrap();
        assert_eq!((cnf.len(), n_vars, n_clauses), (3, 3, 3));

        let (cnf, n_vars, n_clauses) =
            read_dimacs_file("tests/dimacs/dimacs_well_formatted3").unwrap();
        assert_eq!((cnf.len(), n_vars, n_clauses), (0, 20, 0));

        let (cnf, n_vars, n_clauses) =
            read_dimacs_file("tests/dimacs/dimacs_well_formatted4").unwrap();
        assert_eq!((cnf.len(), n_vars, n_clauses), (3, 20, 3));
    }
}
