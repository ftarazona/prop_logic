use crate::representation::proposition_tree::*;
use lexgen::lexer;
use pomelo::pomelo;

pomelo! {
    %include {
        use crate::representation::proposition_tree::*;
    }
    %type input Option<PropositionTree>;
    %type prop PropositionTree;
    %type Literal usize;

    %right Implies;
    %right Equals;
    %left Or;
    %left And;
    %left Xor;
    %left Not;

    input ::= prop?(P) { P };
    prop ::= Literal(N) { PropositionTree::literal(N) }
    prop ::= LPar prop(P) RPar { P }
    prop ::= Not prop(P) { PropositionTree::not(P) }
    prop ::= prop(P1) Xor prop(P2) { PropositionTree::xor(P1, P2) }
    prop ::= prop(P1) And prop(P2) { PropositionTree::and(P1, P2) }
    prop ::= prop(P1) Or prop(P2) { PropositionTree::or(P1, P2) }
    prop ::= prop(P1) Implies prop(P2) { PropositionTree::implication(P1, P2) }
    prop ::= prop(P1) Equals prop(P2) { PropositionTree::equivalence(P1, P2) }
}

use parser::Token;

lexer! {
    PropLexer -> Token;

    let literal = 'x' ['0'-'9']*;

    rule Init   {
        [' ' '\t' '\n' '\r']+,
        '&' => |lex| lex.return_(Token::And),
        '|' => |lex| lex.return_(Token::Or),
        '^' => |lex| lex.return_(Token::Xor),
        "=>" => |lex| lex.return_(Token::Implies),
        "<=>" => |lex| lex.return_(Token::Equals),
        '~' => |lex| lex.return_(Token::Not),
        '(' => |lex| lex.return_(Token::LPar),
        ')' => |lex| lex.return_(Token::RPar),
        $literal => |lex| lex.return_(Token::Literal(
            lex.match_()[1..].to_owned().parse::<usize>().unwrap()
        )),
    }
}

#[derive(Debug)]
pub enum ParserError {
    LexerError,
    ParserError,
    NoPropositionFound,
}

///Returns a proposition tree given a string input.
pub fn parse(input: &str) -> Result<PropositionTree, ParserError> {
    use parser::Parser;

    let mut p = Parser::new();
    let lexer = PropLexer::new(input);

    for tok in lexer {
        match tok {
            Ok((_, tok, _)) => {
                if p.parse(tok).is_err() {
                    return Err(ParserError::ParserError);
                }
            }
            Err(_) => return Err(ParserError::LexerError),
        }
    }

    match p.end_of_input() {
        Ok(Some(p)) => Ok(p),
        Ok(None) => Err(ParserError::NoPropositionFound),
        Err(_) => Err(ParserError::ParserError),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::representation::model::Model;
    use crate::representation::Proposition;

    #[test]
    fn parse_complex_proposition() {
        let prop = parse("(x1 | ~(x2 & x3)) => (x1 <=> x2)").unwrap();
        assert!(prop.eval(&Model::from(&[false, false, false])));
        assert!(prop.eval(&Model::from(&[false, false, true])));
        assert!(!prop.eval(&Model::from(&[false, true, false])));
        assert!(prop.eval(&Model::from(&[false, true, true])));
        assert!(!prop.eval(&Model::from(&[true, false, false])));
        assert!(!prop.eval(&Model::from(&[true, false, true])));
        assert!(prop.eval(&Model::from(&[true, true, false])));
    }

    #[test]
    fn parse_bad_formatted_proposition() {
        assert!(parse("r1 | x2").is_err());
        assert!(parse("x1 + x2 ").is_err());
    }
}
